package com.bosch.codingchallenge.rest.controller;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.env.Environment;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class GoogleSearchControllerSpec {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private Environment env;

    @Test
    public void shouldReturnIAmTeapotMessage() throws Exception {
        String testEndpoint = "http://localhost:" + port + "/search";
        String username = env.getProperty("spring.security.user.username");
        String password = env.getProperty("spring.security.user.password");
        assertThat(testRestTemplate
                .withBasicAuth(username, password)
                .getForObject(testEndpoint, String.class))
                .contains("☕ WE DONT BREW [GET] REQUEST ☕");
    }

}

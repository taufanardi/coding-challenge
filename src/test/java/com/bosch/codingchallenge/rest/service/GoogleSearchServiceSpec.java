package com.bosch.codingchallenge.rest.service;

import com.bosch.codingchallenge.rest.model.GoogleSearchParams;
import com.bosch.codingchallenge.rest.model.GoogleSearchResult;
import com.google.gson.Gson;
import org.apache.http.client.utils.URIBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URISyntaxException;

@RunWith(MockitoJUnitRunner.class)
public class GoogleSearchServiceSpec {

    private final String API_URL = "https://www.googleapis.com/customsearch/v1";
    private GoogleSearchService googleSearchService;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private RestTemplateBuilder restTemplateBuilder;

    @Before
    public void init() {
        Mockito.when(restTemplateBuilder.build()).thenReturn(restTemplate);
        googleSearchService = new GoogleSearchService(restTemplateBuilder);
    }

    @Test
    public void shouldReturnCorrectSearchResult() throws URISyntaxException {
        GoogleSearchParams params = new GoogleSearchParams();
        params.setApiKey("mockKey");
        params.setApiCx("mockCx");
        params.setKeyword("mockKeyword");

        GoogleSearchResult mockResult = new GoogleSearchResult();
        mockResult.setKind("test");
        String mockResultStr = new Gson().toJson(mockResult);

        URIBuilder searchRequestUrl = new URIBuilder(API_URL)
                .addParameter("key", params.getApiKey())
                .addParameter("cx", params.getApiCx())
                .addParameter("q", params.getKeyword())
                .addParameter("num", "10")
                .addParameter("start", "0");

        Mockito.when(restTemplate.getForEntity(searchRequestUrl.build(), String.class))
                .thenReturn(new ResponseEntity<String>(mockResultStr, HttpStatus.OK));

        GoogleSearchResult searchResult = googleSearchService.doSearch(params);
        String searchResultStr = new Gson().toJson(searchResult);
        Assert.assertEquals(mockResultStr, searchResultStr);
    }
}
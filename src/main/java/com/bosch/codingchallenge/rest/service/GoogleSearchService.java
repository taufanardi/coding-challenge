package com.bosch.codingchallenge.rest.service;

import com.bosch.codingchallenge.rest.model.GoogleSearchParams;
import com.bosch.codingchallenge.rest.model.GoogleSearchResult;
import com.google.gson.Gson;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

@Service
public class GoogleSearchService {

    private final String API_URL = "https://www.googleapis.com/customsearch/v1";
    private final RestTemplate restTemplate;

    public GoogleSearchService(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.build();
    }

    public GoogleSearchResult doSearch(GoogleSearchParams params) throws URISyntaxException {
        String keyword = params.getKeyword();
        String key = Optional.ofNullable(params.getApiKey()).orElse("AIzaSyCdmHs6AjM-4osG9MWZ862jUs_3XRbSdWQ");
        String cx = Optional.ofNullable(params.getApiCx()).orElse("da0bc26b8e044994c");

        URIBuilder searchRequestUrl = new URIBuilder(API_URL)
                .addParameter("key", key)
                .addParameter("cx", cx)
                .addParameter("q", keyword)
                .addParameter("num", "10")
                .addParameter("start", "0");
        URI requestUrl = searchRequestUrl.build();

        ResponseEntity<String> response = restTemplate.getForEntity(requestUrl, String.class);
        GoogleSearchResult results = new Gson().fromJson(response.getBody(), GoogleSearchResult.class);

        return response.getStatusCode() == HttpStatus.OK ? results : null;
    }
}
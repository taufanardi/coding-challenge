package com.bosch.codingchallenge.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SearchItem {
    private String kind;
    private String title;
    private String htmlTitle;
    private String link;
    private String displayLink;
    private String snippet;
    private String htmlSnippet;
    private String formattedUrl;
    private String htmlFormattedUrl;
    private Pagemap pagemap;


    // Getter Methods
    public String getKind() {
        return kind;
    }

    public String getTitle() {
        return title;
    }

    public String getHtmlTitle() {
        return htmlTitle;
    }

    public String getLink() {
        return link;
    }

    public String getDisplayLink() {
        return displayLink;
    }

    public String getSnippet() {
        return snippet;
    }

    public String getHtmlSnippet() {
        return htmlSnippet;
    }

    public String getFormattedUrl() {
        return formattedUrl;
    }

    public String getHtmlFormattedUrl() {
        return htmlFormattedUrl;
    }

    public Pagemap getPagemap() {
        return pagemap;
    }

    // Setter Methods
    public void setKind(String kind) {
        this.kind = kind;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setHtmlTitle(String htmlTitle) {
        this.htmlTitle = htmlTitle;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setDisplayLink(String displayLink) {
        this.displayLink = displayLink;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    public void setHtmlSnippet(String htmlSnippet) {
        this.htmlSnippet = htmlSnippet;
    }

    public void setFormattedUrl(String formattedUrl) {
        this.formattedUrl = formattedUrl;
    }

    public void setHtmlFormattedUrl(String htmlFormattedUrl) {
        this.htmlFormattedUrl = htmlFormattedUrl;
    }

    public void setPagemap(Pagemap pagemap) {
        this.pagemap = pagemap;
    }
}

class Pagemap {
    private ArrayList<CseThumbnail> cse_thumbnail = new ArrayList<CseThumbnail>();
    private ArrayList<Metatags> metatags = new ArrayList<Metatags>();
    private ArrayList<CseImage> cse_image = new ArrayList<CseImage>();

    // Getter Methods
    public ArrayList<CseThumbnail> getCse_thumbnail() {
        return cse_thumbnail;
    }

    public ArrayList<Metatags> getMetatags() {
        return metatags;
    }

    public ArrayList<CseImage> getCse_image() {
        return cse_image;
    }

    // Setter Methods
    public void setCse_thumbnail(ArrayList<CseThumbnail> cse_thumbnail) {
        this.cse_thumbnail = cse_thumbnail;
    }

    public void setMetatags(ArrayList<Metatags> metatags) {
        this.metatags = metatags;
    }

    public void setCse_image(ArrayList<CseImage> cse_image) {
        this.cse_image = cse_image;
    }
}

class CseThumbnail {
    private String src;
    private String width;
    private String height;

    // Getter Methods
    public String getSrc() {
        return src;
    }

    public String getWidth() {
        return width;
    }

    public String getHeight() {
        return height;
    }

    // Setter Methods
    public void setSrc(String src) {
        this.src = src;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public void setHeight(String height) {
        this.height = height;
    }
}

class CseImage {
    private String src;

    // Getter Methods
    public String getSrc() {
        return src;
    }

    // Setter Methods
    public void setSrc(String src) {
        this.src = src;
    }
}

class Metatags {
    @SerializedName("application-name")
    private String applicationName;
    @SerializedName("og:image")
    private String ogImage;
    @SerializedName("og:type")
    private String ogType;
    @SerializedName("og:image:width")
    private String ogImageWidth;
    @SerializedName("twitter:card")
    private String twitterCard;
    @SerializedName("twitter:title")
    private String twitterTitle;
    @SerializedName("mod")
    private String mod;
    @SerializedName("og:site_name")
    private String ogSite_name;
    @SerializedName("apple-mobile-web-app-title")
    private String appleMobileWebAppTitle;
    @SerializedName("og:title")
    private String ogTitle;
    @SerializedName("og:image:height")
    private String ogImageHeight;
    @SerializedName("og:description")
    private String ogDescription;
    @SerializedName("twitter:image")
    private String twitterImage;
    @SerializedName("referrer")
    private String referrer;
    @SerializedName("twitter:image:alt")
    private String twitterImageAlt;
    @SerializedName("apple-mobile-web-app-status-bar-style")
    private String appleMobileWebAppStatusBarStyle;
    @SerializedName("msapplication-tap-highlight")
    private String msapplicationTapHighlight;
    @SerializedName("viewport")
    private String viewport;
    @SerializedName("apple-mobile-web-app-capable")
    private String appleMobileWebAppCapable;
    @SerializedName("twitter:description")
    private String twitterDescription;
    @SerializedName("mobile-web-app-capable")
    private String mobileWebAppCapable;
    @SerializedName("og:locale")
    private String ogLocale;
    @SerializedName("og:url")
    private String ogUrl;
}
package com.bosch.codingchallenge.rest.model;

public class GoogleSearchParams {
    private String keyword;
    private String apiKey;
    private String apiCx;

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getApiCx() {
        return apiCx;
    }

    public void setApiCx(String apiCx) {
        this.apiCx = apiCx;
    }

}

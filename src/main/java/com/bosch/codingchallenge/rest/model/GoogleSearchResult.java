package com.bosch.codingchallenge.rest.model;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

public class GoogleSearchResult {
    private String kind;
    private Url url;
    private Queries queries;
    private Context context;
    private SearchInformation searchInformation;
    private ArrayList<SearchItem> items = new ArrayList<SearchItem>();

    // Getter Methods
    public String getKind() {
        return kind;
    }

    public Url getUrl() {
        return url;
    }

    public Queries getQueries() {
        return queries;
    }

    public Context getContext() {
        return context;
    }

    public SearchInformation getSearchInformation() {
        return searchInformation;
    }

    public ArrayList<SearchItem> getItems() {
        return items;
    }

    // Setter Methods
    public void setKind(String kind) {
        this.kind = kind;
    }

    public void setUrl(Url urlObject) {
        this.url = urlObject;
    }

    public void setQueries(Queries queriesObject) {
        this.queries = queriesObject;
    }

    public void setContext(Context contextObject) {
        this.context = contextObject;
    }

    public void setSearchInformation(SearchInformation searchInformationObject) {
        this.searchInformation = searchInformationObject;
    }

    public void setItems(ArrayList<SearchItem> items) {
        this.items = items;
    }
}

class SearchInformation {
    private float searchTime;
    private String formattedSearchTime;
    private String totalResults;
    private String formattedTotalResults;

    // Getter Methods
    public float getSearchTime() {
        return searchTime;
    }

    public String getFormattedSearchTime() {
        return formattedSearchTime;
    }

    public String getTotalResults() {
        return totalResults;
    }

    public String getFormattedTotalResults() {
        return formattedTotalResults;
    }

    // Setter Methods
    public void setSearchTime(float searchTime) {
        this.searchTime = searchTime;
    }

    public void setFormattedSearchTime(String formattedSearchTime) {
        this.formattedSearchTime = formattedSearchTime;
    }

    public void setTotalResults(String totalResults) {
        this.totalResults = totalResults;
    }

    public void setFormattedTotalResults(String formattedTotalResults) {
        this.formattedTotalResults = formattedTotalResults;
    }
}

class Context {
    private String title;

    // Getter Methods
    public String getTitle() {
        return title;
    }

    // Setter Methods
    public void setTitle(String title) {
        this.title = title;
    }
}

class Queries {
    @SerializedName("request")
    private ArrayList<Request> request = new ArrayList<Request>();
    @SerializedName("nextPage")
    private ArrayList<Request> nextPage = new ArrayList<Request>();
}

class Request {
    private String title;
    private String totalResults;
    private String searchTerms;
    private float count;
    private float startIndex;
    private String inputEncoding;
    private String outputEncoding;
    private String safe;
    private String cx;

    // Getter Methods
    public String getTitle() {
        return title;
    }

    public String getTotalResults() {
        return totalResults;
    }

    public String getSearchTerms() {
        return searchTerms;
    }

    public float getCount() {
        return count;
    }

    public float getStartIndex() {
        return startIndex;
    }

    public String getInputEncoding() {
        return inputEncoding;
    }

    public String getOutputEncoding() {
        return outputEncoding;
    }

    public String getSafe() {
        return safe;
    }

    public String getCx() {
        return cx;
    }

    // Setter Methods
    public void setTitle(String title) {
        this.title = title;
    }

    public void setTotalResults(String totalResults) {
        this.totalResults = totalResults;
    }

    public void setSearchTerms(String searchTerms) {
        this.searchTerms = searchTerms;
    }

    public void setCount(float count) {
        this.count = count;
    }

    public void setStartIndex(float startIndex) {
        this.startIndex = startIndex;
    }

    public void setInputEncoding(String inputEncoding) {
        this.inputEncoding = inputEncoding;
    }

    public void setOutputEncoding(String outputEncoding) {
        this.outputEncoding = outputEncoding;
    }

    public void setSafe(String safe) {
        this.safe = safe;
    }

    public void setCx(String cx) {
        this.cx = cx;
    }
}

class Url {
    private String type;
    private String template;

    // Getter Methods
    public String getType() {
        return type;
    }

    public String getTemplate() {
        return template;
    }

    // Setter Methods
    public void setType(String type) {
        this.type = type;
    }

    public void setTemplate(String template) {
        this.template = template;
    }
}
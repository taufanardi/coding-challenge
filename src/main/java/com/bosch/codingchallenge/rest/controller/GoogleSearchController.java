package com.bosch.codingchallenge.rest.controller;

import com.bosch.codingchallenge.rest.model.GoogleSearchParams;
import com.bosch.codingchallenge.rest.model.GoogleSearchResult;
import com.bosch.codingchallenge.rest.model.SearchItem;
import com.bosch.codingchallenge.rest.service.GoogleSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.net.URISyntaxException;
import java.util.ArrayList;

@RestController
@RequestMapping("/search")
public class GoogleSearchController {

    private RestTemplateBuilder restTemplateBuilder;
    private GoogleSearchService searchService;

    @Autowired
    public GoogleSearchController(RestTemplateBuilder builder) {
        this.restTemplateBuilder = builder;
        this.searchService = new GoogleSearchService(this.restTemplateBuilder);
    }

    @RequestMapping(method = RequestMethod.GET)
    public String get() throws ResponseStatusException {
        throw new ResponseStatusException(HttpStatus.I_AM_A_TEAPOT, "☕ WE DONT BREW [GET] REQUEST ☕");
    }

    @RequestMapping(method = RequestMethod.POST)
    public ArrayList<SearchItem> post(@RequestBody GoogleSearchParams params) throws URISyntaxException {
        GoogleSearchResult searchResult = searchService.doSearch(params);
        ArrayList<SearchItem> searchItems = searchResult.getItems();
        return searchItems;
    }
}

coding-challenge
================

Coding challenge for Bosch.

------------------------------------------------------------------------

REST Documentation {#swagger}
------------------

### <http://localhost:8080/swagger-ui.html>

Authentication
----------
This app uses basic auth with following default credential.   
Username: `bosch`   
Password: `bosch`

Development
-----------

-   This is a spring boot application with maven structure.
-   Maven wrapper is included in the repository (/.mvn) so you dont need
    to install it globally.
-   Running application in IDE   
    `1. Create new project from existing source (choose the root project dir)`   
    `2. Resolve the maven dependencies from your IDE`   
    `3. Execute/run CodingChallengeApplication class`   

Google API
----------

-   This app uses google custom search API to make custom search query.   
    <https://developers.google.com/custom-search/v1/overview>   
-   It uses privately generated API key and Search engine ID.   
    By default it uses key below, with limit 100 request per day.   
    Api Key: `AIzaSyCdmHs6AjM-4osG9MWZ862jUs_3XRbSdWQ`   
    Search Engine ID: `da0bc26b8e044994c`   

### \[Optional\]

If the request limit is achieved, or you want to use your own API Key,
follow instruction below:

-   Create Api Key and Search engine ID with your google account here:   
    <https://developers.google.com/custom-search/v1/introduction>
-   Then pass API key and Search ID to this REST app.   
    Check complete doc for /search POST request here:   
    <http://localhost:8080/swagger-ui/index.html#/google-search-controller/post>
